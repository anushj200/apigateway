variable "apigateway_name" {
  description = "The description of the API."
  type        = string
  default     = "dev-http"
}

variable "apigateway_description" {
  description = "The description of the API."
  type        = string
  default     = "My awesome HTTP API Gateway"
}

variable "apigateway_protocol_type" {
  description = "The description of the API."
  type        = string
  default     = "HTTP"
}

variable "get_actions_customer_lambda_arn" {
  description = "The description of the API."
  type        = string
  default     = "arn:aws:lambda:us-east-1:901933808075:function:test123"
}

variable "post_actions_customer_model_lambda_arn" {
  description = "The description of the API."
  type        = string
  default     = "arn:aws:lambda:us-east-1:901933808075:function:test123"
}

variable "post_activity_customer_id_model" {
  description = "The description of the API."
  type        = string
  default     = "arn:aws:lambda:us-east-1:901933808075:function:test123"
}

variable "post_document_id_lambda_arn" {
  description = "The description of the API."
  type        = string
  default     = "arn:aws:lambda:us-east-1:901933808075:function:test123"
}

variable "patch_document_id_lambda_arn" {
  description = "The description of the API."
  type        = string
  default     = "arn:aws:lambda:us-east-1:901933808075:function:test123"
}

variable "delete_document_id_lambda_arn" {
  description = "The description of the API."
  type        = string
  default     = "arn:aws:lambda:us-east-1:901933808075:function:test123"
}

variable "post_auth_cutomer_lambda_arn" {
  description = "The description of the API."
  type        = string
  default     = "arn:aws:lambda:us-east-1:901933808075:function:test123"
}

variable "post_document_extract_customer_document_id_lambda_arn" {
  description = "The description of the API."
  type        = string
  default     = "arn:aws:lambda:us-east-1:901933808075:function:test123"
}

variable "default_lambda_arn" {
  description = "The description of the API."
  type        = string
  default     = "arn:aws:lambda:us-east-1:901933808075:function:test123"
}
