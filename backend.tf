terraform {
  backend "s3" {
    bucket = "terraform-infra-versiond"
    key    = "infra/tfstate11"
    region = "us-east-1"
  }
}
