module "api_gateway" {
  source = "./modules/api_gateway"

  name          = var.apigateway_name
  description   = var.apigateway_description
  protocol_type = var.apigateway_protocol_type

  cors_configuration = {
    #allow_headers = ["content-type", "x-amz-date", "authorization", "x-api-key", "x-amz-security-token", "x-amz-user-agent"]
    allow_methods = ["*"]
    allow_origins = ["*"]
  }

  create_api_domain_name           = false 

  # Custom domain
  #domain_name                 = ""
  #domain_name_certificate_arn = ""

  # Access logs
  #default_stage_access_log_destination_arn = ""
  #default_stage_access_log_format          = "$context.identity.sourceIp - - [$context.requestTime] \"$context.httpMethod $context.routeKey $context.protocol\" $context.status $context.responseLength $context.requestId $context.integrationErrorMessage"

  # Routes and integrations
  integrations = {
    "GET /api/v1/actions/{customer_id}" = {
      lambda_arn             = var.get_actions_customer_lambda_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    }
    "POST /api/v1/actions/{customer_id}/{model}" = {
      lambda_arn             = var.post_actions_customer_model_lambda_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    }
    "POST /api/v1/activities/{customer_id}/{model}" = {
      lambda_arn             = var.post_activity_customer_id_model
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    }
    "POST /api/v1/document/{customer_id}/{document_id}" = {
      lambda_arn             = var.post_document_id_lambda_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    }
    "PATCH /api/v1/document/{customer_id}/{document_id}" = {
      lambda_arn             = var.patch_document_id_lambda_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    }
    "DELETE /api/v1/document/{customer_id}/{document_id}" = {
      lambda_arn             = var.delete_document_id_lambda_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    }
    "POST /api/v1/auth/{customer_id}" = {
      lambda_arn             = var.post_auth_cutomer_lambda_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    }
    "POST /api/v1/document/extract/{customer_id}/{document_id}" = {
      lambda_arn             = var.post_document_extract_customer_document_id_lambda_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    }

    "$default" = {
      lambda_arn = var.default_lambda_arn
    }
  }

  tags = {
    Name = "https-apigateway"
  }
}